# We will asume the system has docker , git 

echo "Stoping previous containers"

# finding id container to stop it
container_id=$(docker ps -q --filter "ancestor=hubuc/backend:hoy")

sudo docker stop $container_id 2>/dev/null

# finding id container to stop it

container_id=$(docker ps -q --filter "ancestor=hubuc/frontend:hoy")

sudo docker stop $container_id 2>/dev/null

cd ../frontend

echo "Building  Docker frontend image"

# building frontend docker image
sudo docker build -t hubuc/frontend:hoy .

sudo docker run -p8012:8012 -t hubuc/frontend:hoy &


echo "Building  Docker backend image"
cd ../backend
sudo docker build -t hubuc/backend:hoy .
sudo docker run -p5001:5001 -t hubuc/backend:hoy &

 echo "************************************************"
 echo "Open Browser on http://127.0.0.1:8012 for testing."
 echo "************************************************"

 browse http://127.0.0.1:8012

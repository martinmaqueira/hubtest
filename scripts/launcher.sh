#!/bin/bash

# Establishing timeout wait in seconds
timeout=5

docker_local(){
	clear
	echo "Deploying locally using Docker."
	bash build-docker-and-deploy.sh
}


kubernetes(){
	clear
	echo "ERROR: Sorry not Implemented."
}

choosedeploy(){
	clear
	select item in "Deploying locally using Docker" "Deploying on Kubernetes"
	do
	case $item in
	"Deploying on Kubernetes")
	kubernetes
	break
	;;
	"Deploying locally using Docker")
	docker_local
	break
	esac
	#done
	done
}

# Request message allowing to stop it
echo "Press any key if you want customized deployment. By default will be deployed locally with Docker Run : (timeout in $timeout seconds):"

# reading stdin with timeout
read -t $timeout -n 1 key

# Check key press

if [[ -z $key ]]; then
  docker_local
else
  choosedeploy
fi


## DevOps Test

The original exercise was:

- Write a small HTTP server that consumes another API like https://open-meteo.com/ and displays the results on a simple web page.

The app is separated into backend and frontend apps to check weather details in a few cities.

For the frontend, I used an HTML template [bootstrap template](https://github.com/mdbootstrap/bootstrap-weather), and for the backend, I used Python with Flask, written from scratch.

To facilitate testing, I also added the option to deploy it locally using Docker.

### Deploying in a single command line

```bash
cd scripts && bash launcher.sh
```

### Deploying in a single command from Git

```bash
git clone git@gitlab.com:martinmaqueira/hubtest.git && cd hubtest/scripts && bash launcher.sh 
```

To test the application, open your browser and enter [http://127.0.0.1:8012](http://127.0.0.1:8012).

### The backend API

The weather service is a simple API that returns the weather details of a city.

[http://127.0.0.1:5001](http://127.0.0.1:5001)

The API endpoint is very simple. If you want to test it with curl, use the following command:

```bash
curl http://127.0.0.1:5001/weather?city=Paris
```

### Assets 
```bash
|-- backend/        (weather service sources: Python/Flask service, Dockerfile.)
|   |-- src/
|   |   |-- [backend service source code]
|   |-- Dockerfile
|
|-- frontend/       (Frontend sources, Dockerfile.)
|   |-- src/
|   |   |-- [frontend service source code]
|   |-- Dockerfile
|
|-- scripts/        (Scripts for deploying locally in Docker with Docker run.)
|   |-- build-docker-and-deploy.sh (script to build Docker image and deploy locally)
|   |-- launcher.sh  (The main script for full deployment.)
|
|-- README.md (project documentation)
```

### Resources
- [Bootstrap Weather Template](https://github.com/mdbootstrap/bootstrap-weather)
- [Open Meteo API](https://open-meteo.com/)
- GitHub Copilot integrated in my PyCharm
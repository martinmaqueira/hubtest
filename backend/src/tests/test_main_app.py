import unittest
from app.main_app import app

class FlaskTestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def test_weather(self):
        response = self.app.get('/weather?city=barcelona')
        self.assertEqual(response.status_code, 200)
        response = dict(response.get_json())
        self.assertIsInstance(response, dict)
        self.assertIn('temperature', response.keys())
        self.assertIn('windspeed', response.keys())
        self.assertIn('elevation', response.keys())
        self.assertIn('is_day', response.keys())
        self.assertIn('error', response.keys())
        self.assertEqual(response['error'], '')
        self.assertIsInstance(response['temperature'], float)
        self.assertIsInstance(response['windspeed'], float)
        self.assertIsInstance(response['elevation'], float)
        self.assertIsInstance(response['is_day'], int)

    def test_weather_error(self):
        response = self.app.get('/weather?city=barcelon')
        self.assertEqual(response.status_code, 200)
        response = dict(response.get_json())
        self.assertIsInstance(response, dict)
        self.assertIn('temperature', response.keys())
        self.assertIn('windspeed', response.keys())
        self.assertIn('elevation', response.keys())
        self.assertIn('is_day', response.keys())
        self.assertIn('error', response.keys())
        self.assertEqual(response['error'], 'City <barcelon> is not in Database.')
        self.assertIsInstance(response['temperature'], float)
        self.assertIsInstance(response['windspeed'], float)
        self.assertIsInstance(response['elevation'], float)
        self.assertIsInstance(response['is_day'], int)

if __name__ == '__main__':
    unittest.main()

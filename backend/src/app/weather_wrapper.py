from cities import cities
import requests


def get_city_temperature_and_more(city: str) -> dict:
    """ This function returns the weather data for a given city """
    standard_response = {
        'temperature': 0.0,
        'windspeed': 0.0,
        'elevation': 0.0,
        'is_day': 0,
        'error': ''
    }
    if city in cities.keys():
        latitude = cities[city]['latitude']
        longitude = cities[city]['longitude']
        time_zone = cities[city]['time_zone']
        url = f'https://api.open-meteo.com/v1/forecast?latitude={latitude}&longitude={longitude}&current_weather=true' \
              f'&forecast_days=1&timezone={time_zone}'
        response = requests.get(url)
        final_response = standard_response
        if response.status_code == 200:
            final_response['temperature'] = response.json()['current_weather']['temperature']
            final_response['windspeed'] = response.json()['current_weather']['windspeed']
            final_response['elevation'] = response.json()['elevation']
            final_response['is_day'] = response.json()['current_weather']['is_day']
        else:
            final_response = standard_response
            final_response['error'] = f'Error getting weather data for {city}'
    else:
        final_response = standard_response
        final_response['error'] = f'City <{city}> is not in Database.'
    return final_response

#!/usr/bin/env python
from flask import Flask, request, jsonify
from flask_cors import CORS
from weather_wrapper import get_city_temperature_and_more
import os

app = Flask(__name__)


# Reading variable from environment
SERVICE_PORT = os.environ.get('SERVICE_PORT',5001)

# We disable CORS check
cors = CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/weather', methods=['get'])
def get_weather_data():
    city = request.args.get('city').lower()
    weather_data = get_city_temperature_and_more(city)
    response = jsonify(weather_data)
    return response


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=SERVICE_PORT)

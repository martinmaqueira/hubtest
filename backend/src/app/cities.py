cities = {
    "barcelona": {
        "latitude": 41.39,
        "longitude": 2.16,
        "time_zone": "Europe/Madrid"
    },
    "paris": {
        "latitude": 48.85,
        "longitude": 2.35,
        "time_zone": "Europe/Paris"
    },
    "buenos aires": {
        "latitude": -34.61,
        "longitude": -58.38,
        "time_zone": "America/Argentina/Buenos_Aires"
    },
    "washington": {
        "latitude": 38.90,
        "longitude": -77.04,
        "time_zone": "America/New_York"
    },
    "cairo": {
        "latitude": 30.06,
        "longitude": 31.25,
        "time_zone": "Africa/Cairo"
    },
    "tokyo": {
        "latitude": 35.68,
        "longitude": 139.77,
        "time_zone": "Asia/Tokyo"
    }
}

